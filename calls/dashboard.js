var request = require('sync-request');
var parseString = require('xml2js').parseString;
require('dotenv').config();


var getPrediction = function(url) {
  var xml = request('GET', url).getBody();
  var waitTime = "";
  parseString(xml, (err, result) => {
    waitTime = result['bustime-response']['prd'][0]['prdctdn'][0];
  });
  if(parseInt(waitTime) === NaN) {
    return waitTime;
  } else {
    return waitTime + " min";
  }
}
var getNorthbound = function() {
  return getPrediction(`http://www.ctabustracker.com/bustime/api/v2/getpredictions?key=${process.env.CTABUS_API_KEY}&rt=22&des=Howard&rtdir=Northbound&stpid=1936`)

};


var getSouthbound = function() {
  return getPrediction(`http://www.ctabustracker.com/bustime/api/v2/getpredictions?key=${process.env.CTABUS_API_KEY}&rt=22&rtdir=Southbound&stpid=1791`)
};

var getRain = function() {
  var json = request('GET', `https://samples.openweathermap.org/data/2.5/weather?lat=87%2E67&lon=42&appid=${process.env.WEATHER_API_KEY}`).getBody();
  return JSON.parse(json)['weather'][0]['description'];
}

module.exports = {
  northBound: getNorthbound,
  southBound: getSouthbound,
  rain: getRain
};
