var express = require('express');
var router = express.Router();
var dash = require('../calls/dashboard');

router.get('/', function(req, res, next) {
  res.render('index', {
    northbound: dash.northBound(),
    southbound: dash.southBound(),
    rain: dash.rain()
  });
});

module.exports = router;
